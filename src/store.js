import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
    state: {
        status: '',
        user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '',
        dark: true,
        baseApi: process.env.VUE_APP_BASE_URI,
        redirectAuthSuccess: process.env.VUE_APP_REDIRECT_AUTH_SUCCESS
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, user) {
            state.status = 'success';
            state.user = user;
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = '';
            state.user = ''
        },
        dark(state, on) {
            state.dark = on;
        },
    },
    actions: {
        login({commit, state}, credentials) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: state.baseApi + '/auth/local', data: credentials, method: 'POST'})
                    .then(resp => {
                        const user = resp.data;
                        localStorage.setItem('user', JSON.stringify(user));
                        axios.defaults.headers.common['Authorization'] = `bearer ${user.jwt}`;
                        commit('auth_success', user);
                        console.log(state.redirectAuthSuccess)

                        window.getApp.$router.push(state.redirectAuthSuccess);
                        window.getApp.$emit('SNACKBAR_SUCCESS', 'Zalogowano użytkownika.');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('user');
                        window.getApp.$emit('SNACKBAR_ERROR', (err.response) ? err.response.data.message : err);
                        reject(err)
                    })
            })
        },
        register({commit, state}, data) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: state.baseApi + '/auth/local/register', data: data, method: 'POST'})
                    .then(resp => {
                        localStorage.removeItem('user');
                        commit('auth_success', '');
                        window.getApp.$router.push('/login');
                        window.getApp.$emit('SNACKBAR_SUCCESS', 'Zarejestrowano użytkownika. Link aktywacyjny wysłany na e-mail.');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('user');
                        window.getApp.$emit('SNACKBAR_ERROR', (err.response) ? err.response.data.message : err);
                        reject(err)
                    })
            })
        },
        confirm({commit, state}, params) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: state.baseApi + '/auth/email-confirmation', params: params, method: 'GET'})
                    .then(resp => {
                        localStorage.removeItem('user');
                        commit('auth_success', '');
                        window.getApp.$router.push('/login');
                        window.getApp.$emit('SNACKBAR_SUCCESS', 'Konto aktywne.');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('user');
                        window.getApp.$emit('SNACKBAR_ERROR', (err.response) ? err.response.data.message : err);
                        reject(err)
                    })
            })
        },
        forgotten({commit, state}, data) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: state.baseApi + '/auth/forgot-password', data: data, method: 'POST'})
                    .then(resp => {
                        localStorage.removeItem('user');
                        commit('auth_success', '');
                        window.getApp.$emit('SNACKBAR_SUCCESS', 'Wysłano na e-mail link odzyskiwania hasła.');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('user');
                        window.getApp.$emit('SNACKBAR_ERROR', (err.response) ? err.response.data.message : err);
                        reject(err)
                    })
            })
        },
        reset({commit, state}, data) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: state.baseApi + '/auth/reset-password', data: data, method: 'POST'})
                    .then(resp => {
                        const user = resp.data;
                        localStorage.setItem('user', JSON.stringify(user));
                        axios.defaults.headers.common['Authorization'] = `bearer ${user.jwt}`;
                        commit('auth_success', user);
                        window.getApp.$router.push(state.redirectAuthSuccess);
                        window.getApp.$emit('SNACKBAR_SUCCESS', 'Zalogowano użytkownika.');
                        resolve(resp);
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('user');
                        window.getApp.$emit('SNACKBAR_ERROR', (err.response) ? err.response.data.message : err);
                        reject(err)
                    })
            })
        },
        logout({commit}) {
            return new Promise((resolve) => {
                commit('logout');
                localStorage.removeItem('user');
                delete axios.defaults.headers.common['Authorization'];
                window.getApp.$router.push('/login');
                window.getApp.$emit('SNACKBAR_SUCCESS', 'Wylogowano użytkownika.');
                resolve()
            })
        },
        dark({commit}, on) {
            return new Promise((resolve) => {
                commit('dark', on);
                resolve()
            })
        },
    },
    getters: {
        isLoggedIn: state => !!state.user,
        authStatus: state => state.status,
        user: state=> (state.user) ? state.user.user : {},
        isDark: state=> state.dark
    }
});
