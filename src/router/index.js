import Vue from 'vue';
import Router from 'vue-router';
import paths from './paths';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import menu from '@/api/menu';
import util from '@/util';

Vue.use(Router);
const router = new Router({
    base: '/',
    mode: 'hash',
    linkActiveClass: 'active',
    routes: paths
});
// router gards
router.beforeEach((to, from, next) => {
    NProgress.start();
    const title = util.breadcrumbs(menu, to).join(' - ');
    document.title = (title) ? `${process.env.VUE_APP_NAME_DISPLAY} - ${title}` : process.env.VUE_APP_NAME_DISPLAY;
    if (to.matched.some(record => !record.meta.public && !record.meta.publicWithLayout)) {
        if (Vue.prototype.store.getters.isLoggedIn) {
            next();
            return
        }
        next('/login')
    } else {
        next()
    }
});

router.afterEach((to, from) => {
    NProgress.done();
});


export default router;
